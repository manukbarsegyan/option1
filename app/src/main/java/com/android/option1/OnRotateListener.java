package com.android.option1;

public interface OnRotateListener {
	void onRotate(float newAngle);
}
