package com.android.option1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final RectangleDrawingView rectangleDrawingView = (RectangleDrawingView) findViewById(R.id.rectangle_drawing_view);
		final TriangleDrawingView triangleDrawingView = (TriangleDrawingView) findViewById(R.id.triangle_drawing_view);
		rectangleDrawingView.setOnRotateListener(new OnRotateListener() {
			@Override
			public void onRotate(float newAngle) {
				((TextView) findViewById(R.id.rotation_preview)).setText(String.valueOf(newAngle));
			}
		});

		triangleDrawingView.setOnRotateListener(new OnRotateListener() {
			@Override
			public void onRotate(float newAngle) {
				((TextView) findViewById(R.id.rotation_preview)).setText(String.valueOf(newAngle));
			}
		});

		Button rectangle = findViewById(R.id.rectangle);
		Button triangle = findViewById(R.id.triangle);
		View.OnClickListener onClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getId() == R.id.rectangle) {
					rectangleDrawingView.setVisibility(View.VISIBLE);
					triangleDrawingView.setVisibility(View.GONE);
					triangleDrawingView.reset();
				} else if (v.getId() == R.id.triangle) {
					rectangleDrawingView.setVisibility(View.GONE);
					triangleDrawingView.setVisibility(View.VISIBLE);
					rectangleDrawingView.reset();
				}
			}
		};
		rectangle.setOnClickListener(onClickListener);
		triangle.setOnClickListener(onClickListener);
	}
}
