package com.android.option1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class RectangleDrawingView extends View {

	Paint paint = new Paint();
	Rect rect = new Rect();

	int rectWidth = 500;
	int rectHeight = 250;

	float previousX;
	float previousY;

	float degress = 360;

	OnRotateListener onRotateListener;

	public RectangleDrawingView(Context context) {
		super(context);
		init();
	}

	public RectangleDrawingView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public RectangleDrawingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public void setOnRotateListener(OnRotateListener onRotateListener) {
		this.onRotateListener = onRotateListener;
	}

	public void reset() {
		degress = 360;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		int left = w / 2 - rectWidth / 2;
		int top = h / 2 - rectHeight / 2;
		rect.set(left, top, left + rectWidth, top + rectHeight);
	}

	private void init() {
		paint.setColor(Color.RED);
		paint.setStrokeWidth(10);
		paint.setStyle(Paint.Style.FILL);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int actionMasked = event.getActionMasked();
		switch (actionMasked) {
			case MotionEvent.ACTION_DOWN:
				previousX = event.getX();
				previousY = event.getY();
				break;
			case MotionEvent.ACTION_MOVE:
				float centerX = getWidth() / 2f;
				float centerY = getHeight() / 2f;
				float radiansNew = (float) Math.atan2(event.getY() - centerY, event.getX() - centerX);
				float radiansPrevious = (float) Math.atan2(previousY - centerY, previousX - centerX);
				degress += (float) Math.toDegrees(radiansNew - radiansPrevious);

				if (degress > 360 || degress < 0) {
					degress = 0;
				}
				onRotateListener.onRotate(degress);
				invalidate();
				previousY = event.getY();
				previousX = event.getX();
				break;
		}
		return true;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.rotate(degress, getWidth() / 2, getHeight() / 2);
		canvas.drawRect(rect, paint);
	}

}
