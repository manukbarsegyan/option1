package com.android.option1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class TriangleDrawingView extends View {

	Paint paint = new Paint();

	int width = 500;

	float previousX;
	float previousY;

	int screenWidth;
	float degress = 360;

	Point topPoint = new Point();
	Point bottomLeftPoint = new Point();
	Point bottomRightPoint = new Point();

	OnRotateListener onRotateListener;

	public TriangleDrawingView(Context context) {
		super(context);
		init();
	}

	public TriangleDrawingView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TriangleDrawingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public void setOnRotateListener(OnRotateListener onRotateListener) {
		this.onRotateListener = onRotateListener;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		topPoint.set(getWidth() / 2, getHeight() / 2 - width / 2);
		bottomLeftPoint.set(getWidth() / 2 - width / 2, getHeight() / 2 + width / 2);
		bottomRightPoint.set(getWidth() / 2 + width / 2, getHeight() / 2 + width / 2);
	}

	private void init() {
		screenWidth = getResources().getDisplayMetrics().widthPixels;
		paint.setColor(Color.RED);
		paint.setStrokeWidth(10);
		paint.setStyle(Paint.Style.STROKE);
	}

	public void reset() {
		degress = 360;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int actionMasked = event.getActionMasked();
		switch (actionMasked) {
			case MotionEvent.ACTION_DOWN:
				previousX = event.getX();
				previousY = event.getY();
				break;
			case MotionEvent.ACTION_MOVE:
				float centerX = getWidth() / 2f;
				float centerY = getHeight() / 2f;
				float radiansNew = (float) Math.atan2(event.getY() - centerY, event.getX() - centerX);
				float radiansPrevious = (float) Math.atan2(previousY - centerY, previousX - centerX);
				degress += (float) Math.toDegrees(radiansNew - radiansPrevious);

				if (degress > 360) {
					degress = 0;
				}
				if (degress < 0)  {
					degress = 0;
				}
				onRotateListener.onRotate(degress);
				invalidate();
				previousY = event.getY();
				previousX = event.getX();
				break;
		}
		return true;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.rotate(degress, getWidth() / 2, getHeight() / 2);
		canvas.drawLine(bottomLeftPoint.x, bottomLeftPoint.y, topPoint.x, topPoint.y, paint);
		canvas.drawLine(topPoint.x, topPoint.y, bottomRightPoint.x, bottomRightPoint.y, paint);
		canvas.drawLine(bottomRightPoint.x, bottomRightPoint.y, bottomLeftPoint.x, bottomLeftPoint.y, paint);

	}

}
